# www.leadepc

The website is built with [Hugo](https://gohugo.io).

To build the website for production: `hugo --theme=leadepc`

For a live development preview: `hugo server --theme=leadepc --buildDrafts`.

After you build the website for production, it will be in the `/public` folder and you can copy and move that static website to the server.
