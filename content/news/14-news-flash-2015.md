---
title: News Flashes 2015
date: 2015-10-10T00:00:00+02:00
country: South Africa
---

LeadEPC is pleased to announce that our Level 3 BBBEE status remain valid until April 2016 and that we are working hard to maintain our status under the new codes.

LeadEPC is proud to announce successful registration for ISO9001:2008 for Project Management, Design Engineering, Procurement and Construction of Electrical, Instrumentation, Mechanical & Piping, including installation, testing and commissioning.  The certification was done by QRS, a worldwide certification body with head office in the United Arab Emirates.

LeadEPC have been awarded another Mechanical & Piping project by Siemens for Tarlton Transnet terminal for a Mechanical and Piping upgrade project.

LeadEPC received confirmation that we were accepted as a member of the South African Flameproof Association.

Following on from ISO9001:2008 certification, LeadEPC is proud to announce successful outcome of the readiness audit for ISO3834.

LeadEPC is accepted as a MEIP (Mechanical, Electrical, Instrumention & Piping) Contractor at ArcelorMittal South Africa for Vanderbijlpark, Newcastle, Vereeniging, Pretoria and Saldanha works.

LeadEPC have been awarded another project in the Middle East for a prestigious client in the Metals Industry for an electrical and control upgrade on a slab heating furnace.  This project will assist to increase our presence in the Middle East.

The project entails HMI upgrade of the furnace, utilizing Factory Talk View software from Rockwell.

LeadEPC signed a Channel Partner Agreement with ABB to become an ABB authorised value provider for ABB drives and controls for mostly Variable Speed Products.

Following this important achievement, we were awarded several projects utilizing ABB variable speed drives for ArcelorMittal Vanderbijlpark, Sasol Secunda and Safripol.

We are excited with the prospects of rolling out this opportunity to include Medium Voltage and cabinet type drives in the agreement as well as service and maintenance of ABB drive Systems.
