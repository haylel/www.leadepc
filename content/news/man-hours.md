---
title: Celebrating 50 000 lost time injury free man hours
date: 2014-10-16T00:00:00+02:00
country: Sierra Leone
---

![](man-hours/169.jpg)

Our project team at the Sierra Rutile mine in Sierra Leone celebrated the achievement of 50 000 of lost time injury (LTI) free man hours. To celebrate this noteworthy achievement, Chris Tolmay, the Construction Manager, awarded the traditional gift of rice to the staff, who were clearly delighted at this surprise award.

LeadEPC have been providing electrical and instrumentation design and installation services to Sierra Rutile since 2012 for the current mine expansion and are continuing to support Sierra Rutile despite the current Ebola epidemic in Sierra Leone.

![](man-hours/180.jpg)
