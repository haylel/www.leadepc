---
title: Middle East control system server upgrade
date: 2014-03-09T00:00:00+02:00
---

Lead EPC is pleased to announce the award of a control system server upgrade project for a key client in the Middle East. The project involves the upgrading of a Rockwell control system server infrastructure to a fully-redundant server architecture and is planned for implementation in November 2014.
