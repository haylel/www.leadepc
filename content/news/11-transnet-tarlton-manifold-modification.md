---
title: Transnet Tarlton Manifold Modification Project
date: 2014-10-08T00:00:00+02:00
---

Lead EPC's Mechanical & Piping Division is proud to announce the award of the “Transnet Tarlton Manifold Modification Project" by Siemens (Proprietary) Limited. This is a further breakthrough for Lead EPC's endeavour to expand it's footprint in the Oil & Gas Industry in Southern, East and West Africa together with the Middle East.

Rudi Rautenbach, an experienced M&P Project Manager, and Chris Rheeder, a well known M&P Construction Manger in the Oil & Gas Industry, will be heading up the Team to execute this project.

The Project consists of various modifications to piping and structure on the existing manifold as well as the installation of a new metering line and De-aerator.

The project will be executed partially in Lead EPC's Fabrication Workshop in Naledi, near Sasolburg, and the final installation and changeover will be done on-site outside Krugersdorp with an anticipated completion date of January 2015.
