---
title: Lead EPC B-BBEE & CIDB Achievements
date: 2014-10-10T00:00:00+02:00
country: South Africa
---

Broad-Based Black Economic Empowerment Verification.

A re-certification audit by SANAS accredited HR Planning for Lead EPC's B-BBEE verification saw us achieving a Level 1 contributor status with an overall point's score of 102.95 and 135% procurement recognition level.

ACHIEVEMENT: LeadEPC was recently awarded with a Level 1 BBBEE Certificate under the new codes with Procurement Recognition Level : 135% and Total Points Scored : 102.95.

View the [BBEE Certificate](bbee-and-cidb-achievements/B-BBEE_certificate_17052016.pdf).

### CIDB Achievements
Likewise the Construction Industry Development board registered Lead EPC with contractor grades 7EB and 5ME.

Both of the above milestones ideally position us for future growth and an ever expanding client base.

View the [CIDB Certificate](bbee-and-cidb-achievements/CIDB_Certificate_2017.pdf).
