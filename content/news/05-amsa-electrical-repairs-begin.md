---
title: AMSA electrical repairs begin
date: 2013-02-19T00:00:00+02:00
---

Lead EPC have started the electrical and instrumentation repairs to the steel making plant at the Arcelor Mittal Vanderbijlpark works which had been damaged as a result of a recent fire.
