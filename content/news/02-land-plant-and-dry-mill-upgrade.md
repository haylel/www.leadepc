---
title: Land Plant & Dry Mill Fines Circuit Upgrade
date: 2014-10-06T00:00:00+02:00
country: Sierra Leone
---

Lead EPC is pleased to announce the successful installation and commissioning of the containerised MV Switchgear over the last two weeks.

The [Shaw Controls](http://www.shawcontrols.co.za/) custom built MV containerised substation comprised 1 and a ½ 6m shipping containers, split lengthwise for shipping and reassembled and joined on site. Thus far the two 1250A incomers and four 750A transformer feeders have been commissioned and the remaining feeders will be commissioned this week.

The design, layout, placement and installation of the substation was an intricate, almost military operation, completed with minimal production interruptions and all credit must go to our site installation and commissioning teams who have worked under tremendous pressure and over an extended period to get the job done!
