---
title: KaXu Solar One water treatment plant control and instrumentation contract award
date: 2013-05-08T00:00:00+02:00
---

Text:
Abeinsa EPC has awarded Lead EPC a contract for the control and instrumentation scope for the water treatment plant at the 100MW KaXu Solar One CSP Plant situated near Pofadder in the Northern Cape.

The engineering scope includes the development of a functional specification, detail engineering drawings and schedules, SCADA screens, and PLC software. The supply and installation scope includes the LV MCC, AC drive, UPS, field instruments and cabling, fibre optic cabling, lighting and small power. We are delighted to have been awarded this contract and to be involved in this exciting project.
