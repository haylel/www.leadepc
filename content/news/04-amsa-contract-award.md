---
title: AMSA contract award
date: 2013-01-23T00:00:00+02:00
---

During january 2013, Lead EPC were awarded their first turnkey contract by Arcelor Mittal South Africa to design, supply and install two 11kV fan starters at the new Blast Furnace D stock house at the Vanderbijlpark Works.
