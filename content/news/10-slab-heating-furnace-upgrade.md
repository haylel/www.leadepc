---
title: Slab Heating Furnace upgrade
date: 2014-09-10T00:00:00+02:00
country: Middle East
---

Lead EPC is pleased to announce the award of a second control system upgrade project for a key client in the Middle East. The project involves the upgrading of an Slab Heating Furnace that supplies heated aluminium slabs to the Hot Strip Mill.

The equipment consists of an Hitachi PLC, 2 Hitachi VSD's and some instrumentation interface devices. A Rockwell PLC and HMI control system will be installed, with ABB VSD's, and commissioning is planned for April 2015. Lead EPC are looking forward to the success of this project as there are a total of 9 furnaces where such upgrades are required.
