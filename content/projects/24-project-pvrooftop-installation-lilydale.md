---
Title: PV Rooftop Installation, Lilydale, KZN
Year: 2015
---

Engineering, Procurement, Construction and Commissioning of a 25kWp rooftop PV installation for Lilydale Dairy in KZN, complete with monitoring station. This is a grid-tied installation utilising Winaico 265Wp panels with SMA STP 25 000 - TL Inverter
