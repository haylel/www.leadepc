---
Title: PV Power Generation for 200 Rural Houses in Phillipstown North Cape
Year: 2016
---

Detail Engineering, Procurement, Installation and Commissioning of small scale PV installations with battery backup for 200 rural houses in the Northern Cape.
