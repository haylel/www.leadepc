---
Title: Conception, Clark, Phillippines (22.98MWp)
Year: 2015
---

Project and Construction management for a 22.98MWp PV plant at CLARK in the 		Philippines for Sterling and Wilson.

The construction management team comprise out of;

-	Project Manager
-	Construction Manager
-	QA/QC Management
-	Safety Management
-	Electrical Supervision
-	Mechanical and Civil Supervision
