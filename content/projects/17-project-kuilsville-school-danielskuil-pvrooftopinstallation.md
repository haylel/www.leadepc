---
Title: PV Rooftop Installation, Kuilsville School, Danielskuil
Year: 2015
---

Engineering, Procurement, Construction and Commissioning of a 41.6kWp rooftop PV installation for Kuilsville school in Danielskuil. Complete with weather station.
