---
Title: PV Rooftop Installation, Oranjeville, Free State
Year: 2015
---

Engineering, Procurement, Construction and Commissioning of a 40kWp rooftop PV installation for a farm warehouse/workshop near Oranjeville in the Free State.

This system is a hybrid with PV, Batteries and Generator. The system supply power to a 	residence and workshop.

The hybrid system is managed with an ABB AC500 PLC. The complete system is engineered by LeadEPC.
