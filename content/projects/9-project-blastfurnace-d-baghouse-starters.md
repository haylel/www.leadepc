---
Title: Blastfurnace D Baghouse 11kV starters
Year: 2013
---

Design, supply, manufacture, install and commission of 2x 11kV ID Fan starters for Arcelor Mittal.
