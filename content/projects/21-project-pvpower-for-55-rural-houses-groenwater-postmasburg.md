---
Title: PV Power Generation for 55 Rural Houses in Groenwater, Postmasburg
Year: 2015
---

Detail Engineering, Procurement, Installation and Commissioning of small scale PV installations with battery backup for 52 rural houses in the Northern Cape.
