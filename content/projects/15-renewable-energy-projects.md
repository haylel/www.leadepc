---
Title: PV and CSP Projects
Year: 2015
---

LeadEPC has a dedicated  team of Engineers and Construction staff executing projects in PV "Photo Voltaic Solutions".
We have also been successful in construction of CSP "Concentrated Solar Power and Biomas projects".
LeadEPC is involved in the development of PV projects throughout Sub-Sahara Africa.
We execute Rooftop PV projects as well as ground based systems.
We have multi-discipline engineering and construction skills including;

- Electrical
- Civil
- Sub-Structure
