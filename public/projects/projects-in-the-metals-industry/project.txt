Title: Projects in the Metals Industry
----
Year: 2015
----
Text: 1.	Relocation of BOF Power Distribution System
Remove existing 400V Main LV Panel from ARC furnaces and re-instate at BOF complete with cabling.

2.	Continuous Caster V2 - Fire Damage
Disaster Recovery repair of fire damage at V2 Continuous Caster. 
Cables had to be replaced within 48 hours after a fire at the Continuous Caster.

3.	AMSA Vanderbijlpark - Hot Strip Mill
Supply and delivery of ABB Run-out table drives.  The drives are rated at 525V, 400kW.
		
4.	AMSA Vanderbijlpark - Hot Strip Mill
R2 Main Drive and R1 Screwdowns ABB drive replacement.
The scope included, Engineering, Supply, Delivery, Installation and Commissioning of new ABB drives during a shutdown.

5.	AMSA Vanderbijlpark - A61 Cooler Fan
Supply of 1MW motor complete with 690VSD and transformer for the Cooler Fan at the Sinter Plant in Vanderbijlpark.
The project include interfacing into the Control System of the existing plant. 
The scope also include MV protection system, new instrumentation and cabling.

6.	AMSA Vereeniging - PLC Replacement
Supply and installation of a new Main PLC panel with remote IO panel for the Degasser Plant.  The solution is based on Siemens S7 PLC equipment.
----