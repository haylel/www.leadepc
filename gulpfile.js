var elixir = require('laravel-elixir');

elixir(function(mix) {
    // compile sass to css
    mix
    // .sass('milligram.sass', 'resources/assets/css')
    // combine all css files into public/css/all.css
    .stylesIn('public/css', 'public/css/all.css')
    // .copy('resources/assets/fonts', 'content/fonts');
    ;
});
